# Product Service

## Setup Axon Server via Docker
```bash
docker run -d --name warehouse-axon-server -p 8024:8024 -p 8124:8124 --hostname warehouse -e AXONSERVER_HOSTNAME=warehouse axoniq/axonserver
```

## Setup PostgreSQL via Docker
```bash
docker run -d --name warehouse-postgresql -p 5432:5432 --hostname warehouse -e POSTGRES_PASSWORD=junk-pwd postgres:11.3
docker exec -it warehouse-postgresql psql -U postgres
create database warehouse_db;
create user warehouse_user with encrypted password 'junk-pwd';
grant all privileges on database warehouse_db to warehouse_user;
exit
```