package org.jtsworkshop.warehouse.product;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.jtsworkshop.warehouse.product.command.IssueProductCommand;
import org.jtsworkshop.warehouse.product.event.ProductIssuedEvent;
import org.jtsworkshop.warehouse.product.exception.NameMissingException;
import org.jtsworkshop.warehouse.product.exception.ProductIdMissingException;
import org.jtsworkshop.warehouse.product.model.ProductAggregate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.axonframework.test.aggregate.FixtureConfiguration;

import java.util.UUID;

public class ProductApplicationTests {

    private FixtureConfiguration<ProductAggregate> fixture;

    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(ProductAggregate.class);
    }


    @Test
    public void giveNoPriorActivity_whenIssueProductCommand_thenShouldPublishProductIssuedEvent() {
        String productId = UUID.randomUUID().toString();
        String name = "Test Product";
        fixture.givenNoPriorActivity()
                .when(new IssueProductCommand(productId, name))
                .expectEvents(new ProductIssuedEvent(productId, name));
    }

    @Test
    public void giveNoPriorActivity_whenIssueProductCommandWithoutName_thenShouldThrowNameMissingExceptionNameMissingException() {
        String productId = UUID.randomUUID().toString();
        fixture.givenNoPriorActivity()
                .when(new IssueProductCommand(productId, null))
                .expectException(NameMissingException.class);
    }

    @Test
    public void giveNoPriorActivity_whenIssueProductCommandWithoutProductId_thenShouldThrowNameMissingException() {
		String name = "Test Product";
        fixture.givenNoPriorActivity()
                .when(new IssueProductCommand(null, name))
                .expectException(ProductIdMissingException.class);
    }

}
