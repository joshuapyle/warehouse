create sequence hibernate_sequence;

create table token_entry
(
    processor_name varchar(255) not null,
    segment integer not null,
    owner varchar(255),
    timestamp varchar(255) not null,
    token oid,
    token_type varchar(255),
    constraint token_entry_pkey
        primary key (processor_name, segment)
);

alter table token_entry owner to warehouse_user;



create table saga_entry
(
    saga_id varchar(255) not null
        constraint saga_entry_pkey
            primary key,
    revision varchar(255),
    saga_type varchar(255),
    serialized_saga oid
);

create table association_value_entry
(
    id bigint not null
        constraint association_value_entry_pkey
            primary key,
    association_key varchar(255) not null,
    association_value varchar(255),
    saga_id varchar(255) not null,
    saga_type varchar(255)
);

alter table association_value_entry owner to warehouse_user;

create index idx_saga_type_association_key_association_value
    on association_value_entry (saga_type, association_key, association_value);

create index idx_saga_id_saga_type
    on association_value_entry (saga_id, saga_type);

