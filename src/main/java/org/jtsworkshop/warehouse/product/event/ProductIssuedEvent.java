package org.jtsworkshop.warehouse.product.event;

import lombok.Data;

@Data
public class ProductIssuedEvent {

    private String productId;
    private String name;

    public ProductIssuedEvent(String productId, String name) {
        this.productId = productId;
        this.name = name;
    }


}
