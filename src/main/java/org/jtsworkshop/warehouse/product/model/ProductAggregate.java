package org.jtsworkshop.warehouse.product.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.jtsworkshop.warehouse.product.command.IssueProductCommand;
import org.jtsworkshop.warehouse.product.event.ProductIssuedEvent;
import org.jtsworkshop.warehouse.product.exception.NameMissingException;
import org.jtsworkshop.warehouse.product.exception.ProductIdMissingException;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Data
@Aggregate
public class ProductAggregate {

    @AggregateIdentifier
    private String id;
    private String name;

    @CommandHandler
    public ProductAggregate(IssueProductCommand cmd) {
        System.out.println("ProductAggregate.ProductAggregate");
        System.out.println("cmd = " + cmd);
        if ( StringUtils.isBlank( cmd.getProductId() ) ) {
            throw new ProductIdMissingException("Product Id is empty");
        }
        if (StringUtils.isBlank( cmd.getName() ) ) {
            throw new NameMissingException("Name is empty");
        }
        apply(new ProductIssuedEvent(cmd.getProductId(), cmd.getName()));
    }

    @EventSourcingHandler
    public void on(ProductIssuedEvent evt) {
        System.out.println("ProductAggregate.on");
        System.out.println("evt = " + evt);
        id = evt.getProductId();
        name = evt.getName();
    }

    protected ProductAggregate() {
    }
    // omitted command handlers and event sourcing handlers
}
