package org.jtsworkshop.warehouse.product.exception;

public class ProductIdMissingException extends RuntimeException {
    public ProductIdMissingException(String message) {
        super(message);
    }
}
