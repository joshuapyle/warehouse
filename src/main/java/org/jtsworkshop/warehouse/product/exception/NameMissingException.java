package org.jtsworkshop.warehouse.product.exception;

public class NameMissingException extends RuntimeException {
    public NameMissingException(String message) {
        super(message);
    }
}
