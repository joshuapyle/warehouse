package org.jtsworkshop.warehouse.product.query;

import lombok.Data;

@Data
public class FindAllProductsPaginatedQuery {
    private int page;
    private int size;
}
