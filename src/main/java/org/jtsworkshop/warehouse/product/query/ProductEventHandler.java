package org.jtsworkshop.warehouse.product.query;

import lombok.RequiredArgsConstructor;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.jtsworkshop.warehouse.product.event.ProductIssuedEvent;
import org.jtsworkshop.warehouse.product.exception.NotFoundException;
import org.jtsworkshop.warehouse.product.persitence.ProductRepository;
import org.jtsworkshop.warehouse.product.persitence.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ProductEventHandler {

    private final ProductRepository productRepository;

    @EventHandler
    public void on(ProductIssuedEvent event) {
        System.out.println("ProductEventHandler.on");
        System.out.println("event = " + event);
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(event.getProductId());
        productEntity.setName(event.getName());
        ProductEntity res = productRepository.save(productEntity);
        System.out.println("res = " + res);
    }

    @QueryHandler
    public Page<ProductEntity> handle(FindAllProductsPaginatedQuery query) {
        System.out.println("ProductEventHandler.handle");
        System.out.println("query = " + query);
        return productRepository.findAll(PageRequest.of(query.getPage(), query.getSize()));
    }

    @QueryHandler
    public ProductEntity handle(FindProductsByIdQuery query) {
        System.out.println("ProductEventHandler.handle");
        System.out.println("query = " + query);
        return productRepository.findById(query.getId())
                .orElseThrow(() -> new NotFoundException(String.format("Product with ID %s could not be found", query.getId())));
    }
}
