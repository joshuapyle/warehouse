package org.jtsworkshop.warehouse.product.query;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class FindProductsByIdQuery {
    private final String id;
}
