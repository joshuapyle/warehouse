package org.jtsworkshop.warehouse.product.command;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@RequiredArgsConstructor
public class IssueProductCommand {

    @TargetAggregateIdentifier
    private final String productId;
    private final String name;
}
