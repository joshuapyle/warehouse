package org.jtsworkshop.warehouse.product.request;

import lombok.Data;

@Data
public class IssueProductRequest {
    private String name;
}
