package org.jtsworkshop.warehouse.product.Controller;

import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.jtsworkshop.warehouse.product.command.IssueProductCommand;
import org.jtsworkshop.warehouse.product.model.ProductAggregate;
import org.jtsworkshop.warehouse.product.persitence.entity.ProductEntity;
import org.jtsworkshop.warehouse.product.query.FindProductsByIdQuery;
import org.jtsworkshop.warehouse.product.request.IssueProductRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    @PostMapping("command/issue")
    public CompletableFuture<String> issueProduct(@RequestBody IssueProductRequest issueProductRequest){
        System.out.println("issueProductRequest = " + issueProductRequest);
        IssueProductCommand commandPayload = new IssueProductCommand(UUID.randomUUID().toString(), issueProductRequest.getName());
        return commandGateway.send(commandPayload);
    }

    @GetMapping("/{id}")
    public CompletableFuture<ProductEntity> getProduct(@PathVariable String id){
        return queryGateway.query(
                new FindProductsByIdQuery(id), ResponseTypes.instanceOf(ProductEntity.class)
        );
    }
}
